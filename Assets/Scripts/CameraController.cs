using UnityEngine;

namespace Com.Openloop.RolyPoly {
    public class CameraController : MonoBehaviour {
        private float sensitivity = 10f;
        
        // Update is called once per frame
        void Update() {
            var mouseWheel = Input.GetAxis("Mouse ScrollWheel");
            Camera.main.orthographicSize += mouseWheel * sensitivity;
        }
    }
}

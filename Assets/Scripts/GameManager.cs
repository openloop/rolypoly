using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using Photon.Pun;
using Photon.Realtime;


namespace Com.Openloop.RolyPoly {
    public class GameManager : MonoBehaviourPunCallbacks {
        public GameObject playerPrefab;
        public AudioManager audioManager;
        const String room = "MultiPlayerRoom";

        void Start() {
            if (PlayerManager.LocalPlayerInstance == null)
            {
                Debug.LogFormat("We are Instantiating LocalPlayer from {0}", SceneManagerHelper.ActiveSceneName);
                // we're in a room. spawn a character for the local player. it gets synced by using PhotonNetwork.Instantiate
                PhotonNetwork.Instantiate(playerPrefab.name, new Vector3(0f, 30f, 0f), Quaternion.identity, 0); 
                //LoadArena();
            } else {
                Debug.LogFormat("Ignoring scene load for {0}", SceneManagerHelper.ActiveSceneName);
            }
            audioManager.StartAudio();
        }

        /// Called when the local player left the room. Load the launcher scene.
        public override void OnLeftRoom() {
            SceneManager.LoadScene(0);
        }

        public void LeaveRoom() {
            PhotonNetwork.LeaveRoom();
        }

        void LoadArena() {
            if (!PhotonNetwork.IsMasterClient) {
                Debug.LogError("PhotonNetwork : Trying to Load a level but we are not the master client");
            } 

            Debug.LogFormat("PhotonNetwork : Loading Level : {0}", room);
            PhotonNetwork.LoadLevel(room);
        }

        public override void OnPlayerEnteredRoom(Player other) {
            // not seen if you're the player connecting
            Debug.LogFormat("OnPlayerEnteredRoom() {0}", other.NickName); 
            // called before OnPlayerLeftRoom
            if (PhotonNetwork.IsMasterClient) {
                Debug.LogFormat("OnPlayerEnteredRoom IsMasterClient {0}", PhotonNetwork.IsMasterClient);
                LoadArena();
            }
        }

        public override void OnPlayerLeftRoom(Player other) {
            // seen when others disconnect
            Debug.LogFormat("OnPlayerLeftRoom() {0}", other.NickName);

            if (PhotonNetwork.IsMasterClient){
                // called before OnPlayerLeftRoom
                Debug.LogFormat("OnPlayerLeftRoom IsMasterClient {0}", PhotonNetwork.IsMasterClient);
                LoadArena();
            }
        }
    }
}
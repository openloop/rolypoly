using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

namespace Com.Openloop.RolyPoly {
    public class TerrainGenerator : MonoBehaviourPunCallbacks {
        public int depth = 10;
        public int width = 256;
        public int height = 256;
        public float scale = 10f;
        public float offsetX = 2f;
        public float offsetY = 2f;

        void Start() {
            //seed random number generator to create the same psuedorandom terrain for players
            Random.InitState(42);
            offsetX = Random.Range(0f, 2f);
            offsetY = Random.Range(0f, 2f);
        }

        void FixedUpdate() {
            Terrain terrain = GetComponent<Terrain>();
            terrain.terrainData = GenerateTerrain(terrain.terrainData);
            offsetX += Time.deltaTime / 64 * 1f;
            offsetY += Time.deltaTime / 64 * 1f;
        }

        TerrainData GenerateTerrain(TerrainData terrainData) {
            terrainData.heightmapResolution = width + 1;
            terrainData.size = new Vector3 (width, depth, height);
            terrainData.SetHeights(0, 0, GenerateHeights());
            return terrainData;
        }

        float[,] GenerateHeights () {
            float[,] heights = new float[width, height];
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    heights[x, y] = CalculateHeight(x, y);
                }
            }
            return heights;
        }

        float CalculateHeight (int x, int y) {
            float xCoord = (float)x / width * scale * offsetX;
            float yCoord = (float)y / height * scale * offsetY;

            return Mathf.PerlinNoise(xCoord, yCoord);
        }
    }
}